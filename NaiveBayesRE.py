# Run this cell! It sets some things up for you.
 
from __future__ import division  # this line is important to avoid unexpected behavior from division
import os
import zipfile
import math
import time
import operator
from collections import defaultdict, Counter
 
 
 
 
 
 
def tokenize_doc(doc):
    """
    Tokenize a document and return its bag-of-words representation.
    doc - a string representing a document.
    returns a dictionary mapping each word to the number of times it appears in doc.
    """
    bow = defaultdict(float)
    tokens = doc.split()
    lowered_tokens = map(lambda t: t.lower(), tokens)
    for token in lowered_tokens:
        bow[token] += 1.0
    return dict(bow)
 
 
 
def n_word_types(word_counts):
    '''
    return a count of all word types in the corpus
    using information from word_counts
    '''
    types = len(word_counts)
  
    return types
 
 
def n_word_tokens(word_counts):
    '''
    return a count of all word tokens in the corpus
    using information from word_counts
    '''
    tokens = 0
    for w in word_counts:
      tokens += word_counts[w]
 
    return tokens
 
cpos = Counter()
cneg = Counter()
c  = Counter()
 
 
import math                 
class NaiveBayes:
    """A Naive Bayes model for text classification."""
 
    def __init__(self, path_to_data, tokenizer, class1, class2):
        # Vocabulary is a set that stores every word seen in the training data
        self.vocab = set()
        self.path_to_data = path_to_data
        self.tokenize_doc = tokenizer
        self.train_dir = os.path.join(path_to_data, "train")
        self.test_dir = os.path.join(path_to_data, "test")
        self.a = class1
        self.b = class2
        # class_total_doc_counts is a dictionary that maps a class (i.e., pos/neg) to
        # the number of documents in the trainning set of that class
        self.class_total_doc_counts = { self.a: 0.0,
                                        self.b: 0.0 }
 
        # class_total_word_counts is a dictionary that maps a class (i.e., pos/neg) to
        # the number of words in the training set in documents of that class
        self.class_total_word_counts = { self.a: 0.0,
                                         self.b: 0.0 }
 
        # class_word_counts is a dictionary of dictionaries. It maps a class (i.e.,
        # pos/neg) to a dictionary of word counts. For example:
        #    self.class_word_counts[self.a]['awesome']
        # stores the number of times the word 'awesome' appears in documents
        # of the positive class in the training documents.
        self.class_word_counts = { self.a: defaultdict(float),
                                   self.b: defaultdict(float) }
 
    def train_model(self):
        """
        This function processes the entire training set using the global PATH
        variable above.  It makes use of the tokenize_doc and update_model
        functions you will implement.
        """
 
        
 
        pos_path = os.path.join(self.train_dir, self.a)
        neg_path = os.path.join(self.train_dir, self.b)
        for (p, label) in [ (pos_path, self.a), (neg_path, self.b) ]:
            for f in os.listdir(p):
                with open(os.path.join(p,f),'r') as doc:
                    content = doc.read()
                    self.tokenize_and_update_model(content, label)
        self.report_statistics_after_training()
 
    def report_statistics_after_training(self):
        """
        Report a number of statistics after training.
        """
 
        print ("REPORTING CORPUS STATISTICS")
        print ("NUMBER OF DOCUMENTS IN POSITIVE CLASS:", self.class_total_doc_counts[self.a])
        print ("NUMBER OF DOCUMENTS IN NEGATIVE CLASS:", self.class_total_doc_counts[self.b])
        print ("NUMBER OF TOKENS IN POSITIVE CLASS:", self.class_total_word_counts[self.a])
        print ("NUMBER OF TOKENS IN NEGATIVE CLASS:", self.class_total_word_counts[self.b])
        print ("VOCABULARY SIZE: NUMBER OF UNIQUE WORDTYPES IN TRAINING CORPUS:", len(self.vocab))
 
    def update_model(self, bow, label):
        """
   
 
        Update internal statistics given a document represented as a bag-of-words
        bow - a map from words to their counts
        label - the class of the document whose bag-of-words representation was input
        This function doesn't return anything but should update a number of internal
        statistics. Specifically, it updates:
          - the internal map that counts, per class, how many times each word was
            seen (self.class_word_counts)
          - the number of words seen for each label (self.class_total_word_counts)
          - the vocabulary seen so far (self.vocab)
          - the number of documents seen of each label (self.class_total_doc_counts)
        """
        for token in bow:
 
          self.class_word_counts[label][token] += bow[token]
          self.class_total_word_counts[label] += bow[token]
 
          self.vocab.add(token)
        
        self.class_total_doc_counts[label] += 1
 
        #print(label)
        
 
    def tokenize_and_update_model(self, doc, label):
        """
   
 
        Tokenizes a document doc and updates internal count statistics.
        doc - a string representing a document.
        label - the sentiment of the document (either postive or negative)
        stop_word - a boolean flag indicating whether to stop word or not
 
        Make sure when tokenizing to lower case all of the tokens!
        """
 
        bow = tokenize_doc(doc)
 
        for word in bow:
            if(label == self.a):
                cpos[word] += 1
            if(label == self.b):
                cneg[word] += 1
            
            c[word] += 1
 
        self.update_model(bow, label)
 
 
    def top_n(self, label, n):
        """
   
        
        Returns the most frequent n tokens for documents with class 'label'.
        """
 
        sorted_dict = sorted(self.class_word_counts[label].items(), key=lambda item: item[1], reverse=True)[:n]
        return sorted_dict
    
 
    def p_word_given_label(self, word, label):
        """
   
 
        Returns the probability of word given label
        according to this NB model.
        """
 
        return self.class_word_counts[label][word] / self.class_total_word_counts[label]
 
 
    def p_word_given_label_and_alpha(self, word, label, alpha):
        """
   
 
        Returns the probability of word given label wrt psuedo counts.
        alpha - pseudocount parameter
        """
 
        numerator = self.class_word_counts[label][word] + alpha
        denominator = self.class_total_word_counts[label] + (alpha * len(self.vocab))
        return numerator / denominator
        
        
 
    def log_likelihood(self, bow, label, alpha):
        """
   
 
        Computes the log likelihood of a set of words given a label and pseudocount.
        bow - a bag of words (i.e., a tokenized document)
        label - either the positive or negative label
        alpha - float; pseudocount parameter
        """
        count = 0
        for word in bow:
          count += math.log(self.p_word_given_label_and_alpha(word, label, alpha))
        return count
 
    def log_prior(self, label):
        """
   
 
        Returns the log prior of a document having the class 'label'.
        """
        total = self.class_total_doc_counts[self.b] + self.class_total_doc_counts[self.a] 
        prior = math.log(self.class_total_doc_counts[label] / total)
        return prior
 
    def unnormalized_log_posterior(self, bow, label, alpha):
        """
   
 
        Computes the unnormalized log posterior (of doc being of class 'label').
        bow - a bag of words (i.e., a tokenized document)
        """
        print("Unnormalized Log Posterior (" + label + "): " + str(self.log_prior(label) + self.log_likelihood(bow, label, alpha)))
        return self.log_prior(label) + self.log_likelihood(bow, label, alpha)
 
 
    def classify(self, bow, alpha):
        """
   
 
        Compares the unnormalized log posterior for doc for both the positive
        and negative classes and returns the either self.a or self.b
        (depending on which resulted in the higher unnormalized log posterior)
        bow - a bag of words (i.e., a tokenized document)
        """
        positive = self.unnormalized_log_posterior(bow, self.a, alpha)
        negative = self.unnormalized_log_posterior(bow, self.b, alpha)
        # print("Positive: " + str(positive), "Negative: " + str(negative))
        if positive > negative:
          return self.a
        else:
          return self.b
        
 
    def likelihood_ratio(self, word, alpha):
        """
        Returns the ratio of P(word|pos) to P(word|neg).
        """
        positive = self.p_word_given_label_and_alpha(word, self.a, alpha)
        negative = self.p_word_given_label_and_alpha(word, self.b, alpha)
        
        return positive / negative
        
 
    def evaluate_classifier_accuracy(self, alpha):
        """
 
        alpha - pseudocount parameter.
        This function should go through the test data, classify each instance and
        compute the accuracy of the classifier (the fraction of classifications
        the classifier gets right.
        """
        correct = 0.0
        total = 0.0
 
        pos_path = os.path.join(self.test_dir, self.a)
        neg_path = os.path.join(self.test_dir, self.b)
        for (p, label) in [ (pos_path, self.a), (neg_path, self.b) ]:
            for f in os.listdir(p):
                with open(os.path.join(p,f),'r') as doc:
                    content = doc.read()
                    bow = self.tokenize_doc(content)
                    if self.classify(bow, alpha) == label:
                        correct += 1.0
                    total += 1.0
        print("Classifier accuracy: " + str(100 * correct / total))
        return 100 * correct / total
 
PATH_TO_DATA = 'data/'  # path to the data directory
 
TRAIN_DIR = os.path.join(PATH_TO_DATA, "train")
TEST_DIR = os.path.join(PATH_TO_DATA, "test")
 
print("=" * 40)
print("Round 1")
print("=" * 40)
print()
 
class1 = "cpp"
class2 = "javapython"
 
 
for label in [class1, class2]:
    print ("Great! You have {} {} sources in {}".format(len(os.listdir(TRAIN_DIR + "/" + label)),label, TRAIN_DIR + "/" + label))
    
 
 
nb = NaiveBayes(PATH_TO_DATA, tokenizer=tokenize_doc, class1 = class1, class2 = class2)
nb.train_model()
 
tests = []
 
for f in os.listdir(TEST_DIR):
    words = []
    with open(os.path.join(TEST_DIR, f), 'r') as doc:
        content = doc.read()
        bow = tokenize_doc(content)
        for word in bow:
            words.append(word)
    tests.append([f,words])
            
resultcpp = []
resultjavapython = []
 
print()
print("DATA")
print()
 
for test in tests:
    print(test[0], end='')
    print(test[1])
    result = nb.classify(test[1], 0.1)
    print("--- Result: " + result)
    print()
 
    if(result == 'javapython'):
        resultjavapython.append(test)
 
 
 
# round 2
 
print("=" * 40)
print("Round 2")
print("=" * 40)
print()
 
class1 = "java"
class2 = "python"
 
for label in [class1, class2]:
    print ("Great! You have {} {} sources in {}".format(len(os.listdir(TRAIN_DIR + "/" + label)),label, TRAIN_DIR + "/" + label))
    
 
nb = NaiveBayes(PATH_TO_DATA, tokenizer=tokenize_doc, class1 = class1, class2 = class2)
nb.train_model()
 
print()
print("DATA")
print()
 
for test in resultjavapython:
    print(test[0], end='')
    print(test[1])
    result = nb.classify(test[1], 0.1)
    print("--- Result: " + result)
    print()